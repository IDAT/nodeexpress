var express = require('express');
var router = express.Router();

var strCreate = '{0} Registro(s) Insertado(s)';
var strRead = '{0} Registro(s) Encontrado(s)';
var strUpdate = '{0} Registro(s) Actualizado(s)';
var strDelete= '{0} Registro(s) Eliminado(s)';

/* GET users listing. */
router.get('/', function(req, res, next) {
	res.setHeader('Content-Type', 'application/json');
	connection.query('SELECT * FROM tbusuarios', function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": false, "message": error}));
	  	} else {
  			res.send(JSON.stringify({"status": true, "message": strRead.replace("{0}", results.length), "data": results}));
	  	}
  	});
});

router.get('/:cod', function(req, res, next) {
	res.setHeader('Content-Type', 'application/json');
	connection.query('SELECT * FROM tbusuarios WHERE usu_codigo = ?', req.params.cod, function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": false, "message": error}));
	  	} else {
  			res.send(JSON.stringify({"status": true, "message": strRead.replace("{0}", results.length), "data": results}));
	  	}
  	});
});

router.post('/', function(req, res, next) {
	let n = req.body.usu_nombre;
    let p = req.body.usu_passwd;
    let d = req.body.usu_descri;
    let e = req.body.usu_email;
    let c = req.body.usu_estcod;
	res.setHeader('Content-Type', 'application/json');
	connection.query('call prcUsuarioInsert(?,?,?,?,?)', [n,p,d,e,c], function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": false, "message": error}));
	  	} else {
  			res.send(JSON.stringify({"status": true, "message": strCreate.replace("{0}", results.affectedRows)}));
	  	}
  	});
});

router.put('/', function(req, res, next) {
	let n = req.body.usu_nombre;
    let p = req.body.usu_passwd;
    let d = req.body.usu_descri;
    let e = req.body.usu_estcod;
    let c = req.body.usu_codigo;
	res.setHeader('Content-Type', 'application/json');
	connection.query('UPDATE tbusuarios SET usu_nombre =?, usu_passwd = ?, usu_descri = ?, usu_estcod = ?  WHERE usu_codigo = ?', [n,p,d,e,c], function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": false, "message": error}));
	  	} else {
  			res.send(JSON.stringify({"status": true, "message": strUpdate.replace("{0}", results.changedRows)}));
	  	}
  	});
});

router.delete('/:cod', function(req, res, next) {
	res.setHeader('Content-Type', 'application/json');
	connection.query('DELETE FROM tbusuarios WHERE usu_codigo = ?', req.params.cod, function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": false, "message": error}));
	  	} else {
  			res.send(JSON.stringify({"status": true, "message": strDelete.replace("{0}", results.affectedRows)}));
	  	}
  	});
});

module.exports = router;
