# Proyecto Servicio Rest

Basado en NodeJS con módulos http-errors + express + path + cookie-parser + morgan + mysql:

## Getting Started

### Express application generator

Use the application generator tool, express-generator, to quickly create an application skeleton.

The express-generator package installs the express command-line tool. Use the following command to do so:

    $ npm install express-generator -g

For example, the following creates an Express app named NodeJsRest. The app will be created in a folder named NodeJsRest in the current working directory and the view engine will be set to Pug:
 
    $ express --view=pug NodeExpress

Then install dependencies:
   
    $ cd NodeExpress
    $ npm install

On MacOS or Linux, run the app with this command:

    $ DEBUG=NodeExpress:* npm start

On Windows, use this command:

    > set DEBUG=NodeExpress:* & npm start

Then load http://localhost:3000/ in your browser to access the app.

## Documentation

<https://expressjs.com/en/starter/generator.html>

## Testing

Servicio Rest preparado para trabajar metodos `GET` `POST` `PUT` `DELETE`


    $ DEBUG=NodeExpress:* npm start.


### Metodo GET

**Request URI:**

`http://localhost:3000/Usuario`

**Response Body:**

    {   
    "message": "2 REGISTRO(S) ENCONTRADO(S)",
    "status": true,
    "data": [
            {
                "usu_codigo": "USU-001",
                "usu_nombre": "ATORRES",
                "usu_descri": "ALEX FERNANDO TORRES CHAHUARA",
                "usu_passwd": "123",
                "usu_email": "d15105@idat.edu.pe",
                "usu_imagen": null,
                "usu_fecreg": "2018-03-22 00:00:00",
                "usu_estcod": 1,
                "usu_estdes": "ACTIVO"
            },
            {
                "usu_codigo": "USU-002",
                "usu_nombre": "CTORRES",
                "usu_descri": "CIELO TORRES ARAUJO",
                "usu_passwd": "123",
                "usu_email": "d15106@idat.edu.pe",
                "usu_imagen": "2018-02-01 00:00:00",
                "usu_fecreg": null,
                "usu_estcod": 1,
                "usu_estdes": "ACTIVO"
            }
            ]
    }


### Metodo POST

**Request URI:**

`http://localhost:3000/Usuario`

**Request Body:**

    {
        "usu_codigo": "USU-003",
        "usu_nombre": "XTORRES",
        "usu_descri": "AXEL TORRES CHAHUARA",
        "usu_passwd": "757",
        "usu_email" : "d15107@idat.edu.pe",
        "usu_imagen": null,
        "usu_fecreg": "2018-03-01",
        "usu_estcod": 1,
        "usu_estdes": null
    }

**Response:**

    {
        "status"  : true,
        "message" : "1 REGISTRO INSERTADO"
    }

### Metodo PUT

**Request URI:**

`http://localhost:3000/Usuario`

**Request Body:**

    {
        "usu_codigo": "USU-003",
        "usu_nombre": "XTORRES",
        "usu_descri": "AXEL TORRES CHUCTAYA",
        "usu_passwd": "757",
        "usu_email" : "d15107@idat.edu.pe",
        "usu_imagen": null,
        "usu_fecreg": "2018-03-01",
        "usu_estcod": 1,
        "usu_estdes": null
    }

**Response:**

    {
        "status"  : true,
        "message" : "1 REGISTRO ACTUALIZADO"
    }

### Metodo DELETE

**Request URI:**

`http://localhost:3000/Usuario/USU-003`

**Response:**

    {
        "status"  : true,
        "message" : "1 REGISTRO ELIMINADO"
    }

## Author

The Example NodeJs Rest is created and maintained by [Alex Torres Chahuara](http://superahacker.blogspot.com). Alex is a senior
web developer.